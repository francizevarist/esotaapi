package com.esotacredit.esotaapi.api.verification.datamodel;

import com.esotacredit.esotaapi.api.verification.objects.Network;
import com.esotacredit.esotaapi.api.verification.objects.ShopStatus;
import com.esotacredit.esotaapi.api.verification.objects.User;

public class VerificationResponse {

    User user;
    Network network;
    ShopStatus shopStatus;

    public VerificationResponse() {}

    public VerificationResponse(VerificationResponseBody verificationResponseBody) {
        this.user = verificationResponseBody.getUser();
        this.network = verificationResponseBody.getNetworkStatus();
        this.shopStatus = verificationResponseBody.getShopStatus();
    }

    public User getUser() {
        return user;
    }

    public Network getNetwork() {
        return network;
    }

    public ShopStatus getShopStatus() {
        return shopStatus;
    }

}
