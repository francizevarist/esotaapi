package com.esotacredit.esotaapi.api.verification.datamodel;

import com.esotacredit.esotaapi.api.verification.objects.Network;
import com.esotacredit.esotaapi.api.verification.objects.ShopStatus;
import com.esotacredit.esotaapi.api.verification.objects.User;

public class VerificationResponseBody {

    private User user;
    private Network networkStatus;
    private ShopStatus shopStatus;

    public VerificationResponseBody() {
    }

    public VerificationResponseBody(User user, Network networkStatus) {
        this.user = user;
        this.networkStatus = networkStatus;
    }

    public VerificationResponseBody(User user, Network networkStatus, ShopStatus shopStatus) {
        this.user = user;
        this.networkStatus = networkStatus;
        this.shopStatus = shopStatus;
    }

    public User getUser() {
        return user;
    }

    public void setUser(User user) {
        this.user = user;
    }

    public Network getNetworkStatus() {
        return networkStatus;
    }

    public void setNetworkStatus(Network networkStatus) {
        this.networkStatus = networkStatus;
    }

    public ShopStatus getShopStatus() {
        return shopStatus;
    }

    public void setShopStatus(ShopStatus shopStatus) {
        this.shopStatus = shopStatus;
    }



}
