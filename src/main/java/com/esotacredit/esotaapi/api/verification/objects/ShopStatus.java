package com.esotacredit.esotaapi.api.verification.objects;

public class ShopStatus {
    private boolean shopStatus;
    private int shopId;

    public ShopStatus() {}
    public ShopStatus(boolean shopStatus) {
        this.shopStatus = shopStatus;
    }

    public ShopStatus(boolean shopStatus, int shopId) {
        this.shopStatus = shopStatus;
        this.shopId = shopId;
    }

    public boolean isShopStatus() {
        return shopStatus;
    }

    public void setShopStatus(boolean shopStatus) {
        this.shopStatus = shopStatus;
    }

    public int getShopId() {
        return shopId;
    }

    public void setShopId(int shopId) {
        this.shopId = shopId;
    }

}
