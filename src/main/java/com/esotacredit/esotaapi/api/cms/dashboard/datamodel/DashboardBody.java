package com.esotacredit.esotaapi.api.cms.dashboard.datamodel;

public class DashboardBody {
    private int userId;

    public DashboardBody() {}

    public DashboardBody(int userId) {
        this.userId = userId;
    }

    public int getUserId() {
        return userId;
    }
}
