package com.esotacredit.esotaapi.api.cms.shops.model;

import com.esotacredit.esotaapi.api.cms.shops.objects.ItemShop;
import com.esotacredit.esotaapi.api.cms.shops.objects.Message;

import java.util.List;

public class ShopResponse {
    private Message message;
    private List<ItemShop> shopList;

    public ShopResponse(ShopResponseBody shopResponseBody) {
        this.message = shopResponseBody.getMessage();
        this.shopList = shopResponseBody.getShopList();
    }

    public Message getMessage() {
        return message;
    }
    public List<ItemShop> getShopList() {
        return shopList;
    }

}
