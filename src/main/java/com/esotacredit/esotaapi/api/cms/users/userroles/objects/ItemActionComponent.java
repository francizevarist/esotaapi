package com.esotacredit.esotaapi.api.cms.users.userroles.objects;

public class ItemActionComponent {
    private int componentId;
    private String componentName;
    private boolean componentState;

    public ItemActionComponent() {}
    public ItemActionComponent(int componentId, String componentName, boolean componentState) {
        this.componentId = componentId;
        this.componentName = componentName;
        this.componentState = componentState;
    }

    public int getComponentId() {
        return componentId;
    }

    public void setComponentId(int componentId) {
        this.componentId = componentId;
    }

    public String getComponentName() {
        return componentName;
    }

    public void setComponentName(String componentName) {
        this.componentName = componentName;
    }

    public boolean isComponentState() {
        return componentState;
    }

    public void setComponentState(boolean componentState) {
        this.componentState = componentState;
    }

}
