package com.esotacredit.esotaapi.api.cms.sales.delivery.models;

import com.esotacredit.esotaapi.api.cms.sales.delivery.objects.ItemDelivery;
import com.esotacredit.esotaapi.api.cms.sales.message.Message;

import java.util.List;

public class DeliveryResponseBody {
    private Message message;
    private List<ItemDelivery> deliveryList;

    public DeliveryResponseBody() {
    }

    public DeliveryResponseBody(Message message, List<ItemDelivery> deliveryList) {
        this.message = message;
        this.deliveryList = deliveryList;
    }

    public DeliveryResponseBody(Message message) {
        this.message = message;
    }

    public DeliveryResponseBody(List<ItemDelivery> deliveryList) {
        this.deliveryList = deliveryList;
    }

    public Message getMessage() {
        return message;
    }

    public void setMessage(Message message) {
        this.message = message;
    }

    public List<ItemDelivery> getDeliveryList() {
        return deliveryList;
    }

    public void setDeliveryList(List<ItemDelivery> deliveryList) {
        this.deliveryList = deliveryList;
    }
}
