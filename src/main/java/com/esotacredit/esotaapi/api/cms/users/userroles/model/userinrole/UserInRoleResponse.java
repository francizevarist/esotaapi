package com.esotacredit.esotaapi.api.cms.users.userroles.model.userinrole;

import com.esotacredit.esotaapi.api.cms.users.message.Message;
import com.esotacredit.esotaapi.api.cms.users.userroles.objects.ItemUserInRole;

import java.util.List;

public class UserInRoleResponse {

    private Message message;
    private List<ItemUserInRole> userInRoleList;

    public UserInRoleResponse(UserInRoleResponseBody userInRoleResponseBody) {
        this.message = userInRoleResponseBody.getMessage();
        this.userInRoleList = userInRoleResponseBody.getUserInRoleList();
    }

    public Message getMessage() {
        return message;
    }

    public List<ItemUserInRole> getUserInRoleList() {
        return userInRoleList;
    }

}
