package com.esotacredit.esotaapi.api.cms.products.ItemApprove;

public class ItemAproveBody {
    private int userId;
    private boolean approve;

    public ItemAproveBody(int userId) {
        this.userId = userId;
    }

    public ItemAproveBody(int userId, boolean approve) {
        this.userId = userId;
        this.approve = approve;
    }

    public int getUserId() {
        return userId;
    }

    public boolean isApprove() {
        return approve;
    }
}
