package com.esotacredit.esotaapi.api.cms.users;

import com.esotacredit.esotaapi.api.cms.users.systemuser.model.UserBody;
import com.esotacredit.esotaapi.api.cms.users.systemuser.model.UserResponse;
import com.esotacredit.esotaapi.api.cms.users.systemuser.objects.ItemUser;
import com.esotacredit.esotaapi.api.cms.users.message.Message;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@CrossOrigin(origins = "*", allowedHeaders = "*")
@RestController
public class UserResource {

    @Autowired
    UserDaoService service;

    @PostMapping("/getalluser")
    public List<ItemUser> getAllUsers(@RequestBody UserBody userBody){
        return new UserResponse(service.getAllUser(userBody)).getUserList();
    }

    @PostMapping("/blockuser")
    public Message blockUser(@RequestBody UserBody userBody){
        return new UserResponse(service.blockUser(userBody)).getMessage();
    }

    @PostMapping("/deleteuser")
    public Message deleteUser(@RequestBody UserBody userBody){
        return new UserResponse(service.deleteUser(userBody)).getMessage();
    }

}
