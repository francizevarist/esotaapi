package com.esotacredit.esotaapi.api.cms.users.userroles.model.userinrole;

import com.esotacredit.esotaapi.api.cms.users.message.Message;
import com.esotacredit.esotaapi.api.cms.users.userroles.objects.ItemUserInRole;

import java.util.List;

public class UserInRoleResponseBody {
    private Message message;
    private List<ItemUserInRole> userInRoleList;

    public UserInRoleResponseBody() {}
    public UserInRoleResponseBody(Message message, List<ItemUserInRole> userInRoleList) {
        this.message = message;
        this.userInRoleList = userInRoleList;
    }

    public Message getMessage() {
        return message;
    }

    public void setMessage(Message message) {
        this.message = message;
    }

    public List<ItemUserInRole> getUserInRoleList() {
        return userInRoleList;
    }

    public void setUserInRoleList(List<ItemUserInRole> userInRoleList) {
        this.userInRoleList = userInRoleList;
    }

}
