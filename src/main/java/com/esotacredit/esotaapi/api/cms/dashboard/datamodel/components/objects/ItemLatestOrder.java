package com.esotacredit.esotaapi.api.cms.dashboard.datamodel.components.objects;

public class ItemLatestOrder {
    private int productId;
    private String productName;
    private int quantity;
    private String amount;
    private String dateAndTime;
    private String userFullName;
    private String phoneNumber;
    private String paymentStatus;
    private String shopName;

    public ItemLatestOrder() {}

    public ItemLatestOrder(int productId, String productName, int quantity, String amount, String dateAndTime, String userFullName, String phoneNumber, String paymentStatus, String shopName) {
        this.productId = productId;
        this.productName = productName;
        this.quantity = quantity;
        this.amount = amount;
        this.dateAndTime = dateAndTime;
        this.userFullName = userFullName;
        this.phoneNumber = phoneNumber;
        this.paymentStatus = paymentStatus;
        this.shopName = shopName;
    }

    public int getProductId() {
        return productId;
    }

    public void setProductId(int productId) {
        this.productId = productId;
    }

    public String getProductName() {
        return productName;
    }

    public void setProductName(String productName) {
        this.productName = productName;
    }

    public int getQuantity() {
        return quantity;
    }

    public void setQuantity(int quantity) {
        this.quantity = quantity;
    }

    public String getAmount() {
        return amount;
    }

    public void setAmount(String amount) {
        this.amount = amount;
    }

    public String getDateAndTime() {
        return dateAndTime;
    }

    public void setDateAndTime(String dateAndTime) {
        this.dateAndTime = dateAndTime;
    }

    public String getUserFullName() {
        return userFullName;
    }

    public void setUserFullName(String userFullName) {
        this.userFullName = userFullName;
    }

    public String getPhoneNumber() {
        return phoneNumber;
    }

    public void setPhoneNumber(String phoneNumber) {
        this.phoneNumber = phoneNumber;
    }

    public String getPaymentStatus() {
        return paymentStatus;
    }

    public void setPaymentStatus(String paymentStatus) {
        this.paymentStatus = paymentStatus;
    }

    public String getShopName() {
        return shopName;
    }

    public void setShopName(String shopName) {
        this.shopName = shopName;
    }
}
