package com.esotacredit.esotaapi.api.cms.sales.sold.models;

public class SoldBody {
    private int userId;

    public SoldBody() {}

    public SoldBody(int userId) {
        this.userId = userId;
    }

    public int getUserId() {
        return userId;
    }

    public void setUserId(int userId) {
        this.userId = userId;
    }

}
