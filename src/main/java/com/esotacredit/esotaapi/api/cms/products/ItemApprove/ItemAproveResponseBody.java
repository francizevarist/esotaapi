package com.esotacredit.esotaapi.api.cms.products.ItemApprove;

import com.esotacredit.esotaapi.api.cms.products.ItemApprove.objects.ItemToApprove;
import com.esotacredit.esotaapi.api.cms.products.ItemApprove.objects.Message;

import java.util.List;

public class ItemAproveResponseBody {
    private Message message;
    private List<ItemToApprove> itemToApproveList;

    public ItemAproveResponseBody() {}
    public ItemAproveResponseBody(Message message) {
        this.message = message;
    }

    public ItemAproveResponseBody(List<ItemToApprove> itemToApproveList) {
        this.itemToApproveList = itemToApproveList;
    }

    public ItemAproveResponseBody(Message message, List<ItemToApprove> itemToApproveList) {
        this.message = message;
        this.itemToApproveList = itemToApproveList;
    }

    public Message getMessage() {
        return message;
    }

    public void setMessage(Message message) {
        this.message = message;
    }

    public List<ItemToApprove> getItemToApproveList() {
        return itemToApproveList;
    }

    public void setItemToApproveList(List<ItemToApprove> itemToApproveList) {
        this.itemToApproveList = itemToApproveList;
    }
}
