package com.esotacredit.esotaapi.api.cms.dashboard;

import com.esotacredit.esotaapi.api.cms.dashboard.datamodel.DashboardBody;
import com.esotacredit.esotaapi.api.cms.dashboard.datamodel.DashboardResponse;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

@CrossOrigin(origins = "*", allowedHeaders = "*")
@RestController
public class DashboardResource {

    @Autowired
    DashboardDaoService service;
    @PostMapping("/dashboard")
    public DashboardResponse adminDashboard(@RequestBody DashboardBody dashboardBody){
        return new DashboardResponse(service.getDashboard(dashboardBody));
    }

}
