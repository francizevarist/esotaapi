package com.esotacredit.esotaapi.api.cms.products.ItemApprove;

import com.esotacredit.esotaapi.api.cms.products.ItemApprove.objects.ItemToApprove;
import com.esotacredit.esotaapi.api.cms.products.ItemApprove.objects.Message;

import java.util.List;

public class ItemAproveResponse {
    private Message message;
    private List<ItemToApprove> itemsToApprove;

    public ItemAproveResponse(ItemAproveResponseBody itemAproveResponseBody) {
        this.message = itemAproveResponseBody.getMessage();
        this.itemsToApprove = itemAproveResponseBody.getItemToApproveList();
    }

    public Message getMessage() {
        return message;
    }

    public List<ItemToApprove> getItemsToApprove() {
        return itemsToApprove;
    }
}
