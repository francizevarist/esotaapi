package com.esotacredit.esotaapi.api.cms.users.userroles.model.userrole;

import com.esotacredit.esotaapi.api.cms.users.message.Message;
import com.esotacredit.esotaapi.api.cms.users.userroles.objects.ItemUserRole;

import java.util.List;

public class UserRoleResponse {
    private Message message;
    private List<ItemUserRole> userRoleList;

    public UserRoleResponse(UserRoleResponseBody userRoleResponseBody) {
        this.message = userRoleResponseBody.getMessage();
        this.userRoleList = userRoleResponseBody.getUserRoleList();
    }

    public Message getMessage() {
        return message;
    }

    public List<ItemUserRole> getUserRoleList() {
        return userRoleList;
    }

}
