package com.esotacredit.esotaapi.api.cms.users.userroles.model.roleaction;

import com.esotacredit.esotaapi.api.cms.users.message.Message;
import com.esotacredit.esotaapi.api.cms.users.userroles.objects.ItemAction;

import java.util.List;

public class RoleActionResponseBody {
    private Message message;
    private List<ItemAction> actionList;

    public RoleActionResponseBody() {}

    public RoleActionResponseBody(Message message, List<ItemAction> actionList) {
        this.message = message;
        this.actionList = actionList;
    }

    public RoleActionResponseBody(Message message) {
        this.message = message;
    }

    public RoleActionResponseBody(List<ItemAction> actionList) {
        this.actionList = actionList;
    }

    public Message getMessage() {
        return message;
    }

    public void setMessage(Message message) {
        this.message = message;
    }

    public List<ItemAction> getActionList() {
        return actionList;
    }

    public void setActionList(List<ItemAction> actionList) {
        this.actionList = actionList;
    }
}
