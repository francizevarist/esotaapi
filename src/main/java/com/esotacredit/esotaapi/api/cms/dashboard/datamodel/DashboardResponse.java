package com.esotacredit.esotaapi.api.cms.dashboard.datamodel;

import com.esotacredit.esotaapi.api.cms.dashboard.datamodel.components.*;

public class DashboardResponse {
    private Categories categorySize;
    private Credits creditSize;
    private Customers customerSize;
    private Products productSize;
    private TotalSells totalSellsSize;
    private Users userSize;

    public DashboardResponse(DashboardResponseBody dashboardResponseBody) {
        this.categorySize = dashboardResponseBody.getCategorySize();
        this.creditSize = dashboardResponseBody.getCreditSize();
        this.customerSize = dashboardResponseBody.getCustomerSize();
        this.productSize = dashboardResponseBody.getProductSize();
        this.totalSellsSize = dashboardResponseBody.getTotalSellsSize();
        this.userSize = dashboardResponseBody.getUserSize();
    }

    /**JSON Response
     * get categories size
     * get product size
     * get customer size
     * get system user size
     * get credit list size
     * get total sells size
     */
    public Categories getCategorySize() {
        return categorySize;
    }

    public Credits getCreditSize() {
        return creditSize;
    }

    public Customers getCustomerSize() {
        return customerSize;
    }

    public Products getProductSize() {
        return productSize;
    }

    public TotalSells getTotalSellsSize() {
        return totalSellsSize;
    }

    public Users getUserSize() {
        return userSize;
    }

}
