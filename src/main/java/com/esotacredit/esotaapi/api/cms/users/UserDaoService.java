package com.esotacredit.esotaapi.api.cms.users;

import com.esotacredit.esotaapi.api.cms.users.systemuser.model.UserBody;
import com.esotacredit.esotaapi.api.cms.users.systemuser.model.UserResponseBody;
import com.esotacredit.esotaapi.api.cms.users.systemuser.objects.ItemUser;
import com.esotacredit.esotaapi.api.cms.users.message.Message;
import org.springframework.stereotype.Component;

import java.util.List;

@Component
public class UserDaoService {

    UserResponseBody getAllUser(UserBody userBody){
        List<ItemUser> userList = null;
        return new UserResponseBody(userList);
    }

    UserResponseBody blockUser(UserBody userBody){
        String message = null;
        return new UserResponseBody(new Message(message));
    }

    UserResponseBody deleteUser(UserBody userBody){
        String message = null;
        return new UserResponseBody(new Message(message));
    }

}
