package com.esotacredit.esotaapi.api.cms.sales.delivery.models;

import com.esotacredit.esotaapi.api.cms.sales.delivery.objects.ItemDelivery;
import com.esotacredit.esotaapi.api.cms.sales.message.Message;

import java.util.List;

public class DeliveryResponse {
    private Message message;
    private List<ItemDelivery> deliveryList;

    public DeliveryResponse(DeliveryResponseBody deliveryResponseBody) {
        this.message = deliveryResponseBody.getMessage();
        this.deliveryList = deliveryResponseBody.getDeliveryList();
    }

    public Message getMessage() {
        return message;
    }

    public List<ItemDelivery> getDeliveryList() {
        return deliveryList;
    }
}
