package com.esotacredit.esotaapi.api.cms.shops;

import com.esotacredit.esotaapi.api.cms.shops.model.ShopBody;
import com.esotacredit.esotaapi.api.cms.shops.model.ShopResponseBody;
import com.esotacredit.esotaapi.api.cms.shops.objects.ItemShop;
import com.esotacredit.esotaapi.api.cms.shops.objects.Message;
import org.springframework.stereotype.Component;

import java.util.List;

@Component
public class ShopDaoService {

    public ShopResponseBody getAllShops(ShopBody shopBody){
        List<ItemShop> itemShopList = null;
        return new ShopResponseBody(itemShopList);
    }

    public ShopResponseBody addShop(ShopBody shopBody){
        String message = null;
        return new ShopResponseBody(new Message(message));
    }

    public ShopResponseBody deleteShop(ShopBody shopBody){
        String message = null;
        return  new ShopResponseBody(new Message(message));
    }

    public ShopResponseBody editShop(ShopBody shopBody){
        String message = null;
        return new ShopResponseBody(new Message(message));
    }

}
