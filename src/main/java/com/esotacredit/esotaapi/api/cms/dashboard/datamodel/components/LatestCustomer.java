package com.esotacredit.esotaapi.api.cms.dashboard.datamodel.components;

import com.esotacredit.esotaapi.api.cms.dashboard.datamodel.components.objects.ItemLatestCustomer;

import java.util.List;

public class LatestCustomer {
    private List<ItemLatestCustomer> latestCustomerList;

    public LatestCustomer() {}

    public LatestCustomer(List<ItemLatestCustomer> latestCustomerList) {
        this.latestCustomerList = latestCustomerList;
    }

    public List<ItemLatestCustomer> getLatestCustomerList() {
        return latestCustomerList;
    }

    public void setLatestCustomerList(List<ItemLatestCustomer> latestCustomerList) {
        this.latestCustomerList = latestCustomerList;
    }
}
