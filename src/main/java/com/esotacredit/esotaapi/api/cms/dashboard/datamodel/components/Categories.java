package com.esotacredit.esotaapi.api.cms.dashboard.datamodel.components;

public class Categories {
    private int categorySize;

    public Categories() {}
    public Categories(int categorySize) {
        this.categorySize = categorySize;
    }

    public int getCategorySize() {
        return categorySize;
    }
}
