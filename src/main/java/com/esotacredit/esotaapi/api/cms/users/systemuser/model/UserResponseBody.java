package com.esotacredit.esotaapi.api.cms.users.systemuser.model;

import com.esotacredit.esotaapi.api.cms.users.systemuser.objects.ItemUser;
import com.esotacredit.esotaapi.api.cms.users.message.Message;

import java.util.List;

public class UserResponseBody {
    private Message message;
    private List<ItemUser> userList;

    public UserResponseBody() {}

    public UserResponseBody(Message message, List<ItemUser> userList) {
        this.message = message;
        this.userList = userList;
    }

    public UserResponseBody(Message message) {
        this.message = message;
    }

    public UserResponseBody(List<ItemUser> userList) {
        this.userList = userList;
    }

    public Message getMessage() {
        return message;
    }

    public void setMessage(Message message) {
        this.message = message;
    }

    public List<ItemUser> getUserList() {
        return userList;
    }

    public void setUserList(List<ItemUser> userList) {
        this.userList = userList;
    }
}
