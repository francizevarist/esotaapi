package com.esotacredit.esotaapi.api.cms.dashboard.datamodel.components.objects;

public class ItemLatestCustomer {
    private int    productId;
    private String productName;
    private int    quantity;
    private String productAmount;
    private String dateAndTime;
    private String userFullName;
    private String phoneNumber;
    private String paymentStatus;
    private String shopName;

    public ItemLatestCustomer() {}

    public ItemLatestCustomer(int productId, String productName, int quantity, String productAmount, String dateAndTime, String userFullName, String phoneNumber, String paymentStatus, String shopName) {
        this.productId = productId;
        this.productName = productName;
        this.quantity = quantity;
        this.productAmount = productAmount;
        this.dateAndTime = dateAndTime;
        this.userFullName = userFullName;
        this.phoneNumber = phoneNumber;
        this.paymentStatus = paymentStatus;
        this.shopName = shopName;
    }

    public int getProductId() {
        return productId;
    }

    public void setProductId(int productId) {
        this.productId = productId;
    }

    public String getProductName() {
        return productName;
    }

    public void setProductName(String productName) {
        this.productName = productName;
    }

    public int getQuantity() {
        return quantity;
    }

    public void setQuantity(int quantity) {
        this.quantity = quantity;
    }

    public String getProductAmount() {
        return productAmount;
    }

    public void setProductAmount(String productAmount) {
        this.productAmount = productAmount;
    }

    public String getDateAndTime() {
        return dateAndTime;
    }

    public void setDateAndTime(String dateAndTime) {
        this.dateAndTime = dateAndTime;
    }

    public String getUserFullName() {
        return userFullName;
    }

    public void setUserFullName(String userFullName) {
        this.userFullName = userFullName;
    }

    public String getPhoneNumber() {
        return phoneNumber;
    }

    public void setPhoneNumber(String phoneNumber) {
        this.phoneNumber = phoneNumber;
    }

    public String getPaymentStatus() {
        return paymentStatus;
    }

    public void setPaymentStatus(String paymentStatus) {
        this.paymentStatus = paymentStatus;
    }

    public String getShopName() {
        return shopName;
    }

    public void setShopName(String shopName) {
        this.shopName = shopName;
    }


}
