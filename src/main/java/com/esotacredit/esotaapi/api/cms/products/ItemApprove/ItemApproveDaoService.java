package com.esotacredit.esotaapi.api.cms.products.ItemApprove;

import com.esotacredit.esotaapi.api.cms.products.ItemApprove.objects.ItemToApprove;
import com.esotacredit.esotaapi.api.cms.products.ItemApprove.objects.Message;
import com.esotacredit.esotaapi.database.DatabaseConfiguration;
import org.springframework.stereotype.Component;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.List;

@Component
public class ItemApproveDaoService {

    ItemAproveResponseBody getItemToApprove(ItemAproveBody itemAproveBody){
        List<ItemToApprove> itemToApproveList=null;

        Connection con = DatabaseConfiguration.getDatabeConnection();
        try {

            //category count
            String queryItemToAprove = "";
            Statement stmt_itemToAprove = con.createStatement();
            ResultSet rs_itemToAprove =stmt_itemToAprove.executeQuery(queryItemToAprove);

            while(rs_itemToAprove.next()){
            }

            con.close();
        } catch (SQLException e) {
            e.printStackTrace();
        }

        return new ItemAproveResponseBody(itemToApproveList);
    }

    ItemAproveResponseBody approveItem(ItemAproveBody itemAproveBody){
        String message = null;
        String queryAprove = null;

        Connection con = DatabaseConfiguration.getDatabeConnection();
        try {

            //category count
            if(itemAproveBody.isApprove()){
                queryAprove = "";
            }else {
                queryAprove = "";
            }

            Statement stmt_itemToAprove = con.createStatement();
            ResultSet rs_itemToAprove =stmt_itemToAprove.executeQuery(queryAprove);

            while(rs_itemToAprove.next()){
            }

            con.close();
        } catch (SQLException e) {
            e.printStackTrace();
        }

        return new ItemAproveResponseBody(new Message(message));
    }

}
