package com.esotacredit.esotaapi.api.cms.dashboard.datamodel.components;

public class Credits {
    private int creditSize;

    public Credits() {}

    public Credits(int creditSize) {
        this.creditSize = creditSize;
    }

    public int getCreditSize() {
        return creditSize;
    }
}
