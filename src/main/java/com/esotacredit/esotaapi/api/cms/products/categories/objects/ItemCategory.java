package com.esotacredit.esotaapi.api.cms.products.categories.objects;

public class ItemCategory {
    private int Id;
    private String name;
    private String tagline;

    public ItemCategory() {}
    public ItemCategory(int id, String name, String tagline) {
        Id = id;
        this.name = name;
        this.tagline = tagline;
    }

    public int getId() {
        return Id;
    }

    public void setId(int id) {
        Id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getTagline() {
        return tagline;
    }

    public void setTagline(String tagline) {
        this.tagline = tagline;
    }
}
