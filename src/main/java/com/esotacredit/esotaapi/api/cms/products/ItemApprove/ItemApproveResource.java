package com.esotacredit.esotaapi.api.cms.products.ItemApprove;

import com.esotacredit.esotaapi.api.cms.products.ItemApprove.objects.ItemToApprove;
import com.esotacredit.esotaapi.api.cms.products.ItemApprove.objects.Message;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@CrossOrigin(origins = "*", allowedHeaders = "*")
@RestController
public class ItemApproveResource {

    @Autowired
    ItemApproveDaoService service;

    @PostMapping("/itemstoapprove")
    List<ItemToApprove> itemToApproveList(@RequestBody ItemAproveBody itemAproveBody){
        return  new ItemAproveResponse(service.getItemToApprove(itemAproveBody)).getItemsToApprove();
    }

    @PostMapping("/approveitem")
    Message getMessage(@RequestBody ItemAproveBody itemAproveBody){
        return new ItemAproveResponse(service.approveItem(itemAproveBody)).getMessage();
    }

}
