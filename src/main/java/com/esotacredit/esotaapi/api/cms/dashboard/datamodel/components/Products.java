package com.esotacredit.esotaapi.api.cms.dashboard.datamodel.components;

public class Products {
    private int products;

    public Products() {}

    public Products(int products) {
        this.products = products;
    }

    public int getProducts() {
        return products;
    }
}
