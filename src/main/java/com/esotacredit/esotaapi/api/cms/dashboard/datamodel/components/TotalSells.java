package com.esotacredit.esotaapi.api.cms.dashboard.datamodel.components;

public class TotalSells {
    private int totalSize;

    public TotalSells() {}
    public TotalSells(int totalSize) {
        this.totalSize = totalSize;
    }

    public int getTotalSize() {
        return totalSize;
    }

}
