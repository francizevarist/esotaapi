package com.esotacredit.esotaapi.api.cms.products.categories;

import com.esotacredit.esotaapi.api.cms.products.categories.objects.ItemCategory;
import com.esotacredit.esotaapi.api.cms.products.categories.objects.Message;

import java.util.List;

public class CategoryResponse {
    private Message message;
    private List<ItemCategory> categoryList;

    public CategoryResponse(CategoryResponseBody categoryResponseBody) {
        this.message = categoryResponseBody.getMessage();
        this.categoryList =  categoryResponseBody.getCategoryList();
    }

    public Message getMessage() {
        return message;
    }

    public List<ItemCategory> getCategoryList() {
        return categoryList;
    }

}
