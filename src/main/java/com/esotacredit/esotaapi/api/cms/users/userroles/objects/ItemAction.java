package com.esotacredit.esotaapi.api.cms.users.userroles.objects;

import java.util.List;

public class ItemAction {
    private int Id;
    private String ActionTitle;
    private List<ItemActionComponent> actionComponentList;

    public ItemAction() {}

    public ItemAction(int id, String actionTitle, List<ItemActionComponent> actionComponentList) {
        Id = id;
        ActionTitle = actionTitle;
        this.actionComponentList = actionComponentList;
    }

    public int getId() {
        return Id;
    }

    public void setId(int id) {
        Id = id;
    }

    public String getActionTitle() {
        return ActionTitle;
    }

    public void setActionTitle(String actionTitle) {
        ActionTitle = actionTitle;
    }

    public List<ItemActionComponent> getActionComponentList() {
        return actionComponentList;
    }

    public void setActionComponentList(List<ItemActionComponent> actionComponentList) {
        this.actionComponentList = actionComponentList;
    }
}
