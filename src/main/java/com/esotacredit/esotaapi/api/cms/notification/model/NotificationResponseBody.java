package com.esotacredit.esotaapi.api.cms.notification.model;

import com.esotacredit.esotaapi.api.cms.notification.objects.QuickInfo;

public class NotificationResponseBody {
    private QuickInfo quickInfo;

    public NotificationResponseBody(QuickInfo quickInfo) {
        this.quickInfo = quickInfo;
    }

    public QuickInfo getQuickInfo() {
        return quickInfo;
    }

    public void setQuickInfo(QuickInfo quickInfo) {
        this.quickInfo = quickInfo;
    }

}
