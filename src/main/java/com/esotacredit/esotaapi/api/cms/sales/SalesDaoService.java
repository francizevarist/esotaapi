package com.esotacredit.esotaapi.api.cms.sales;

import com.esotacredit.esotaapi.api.cms.sales.delivery.models.DeliveryBody;
import com.esotacredit.esotaapi.api.cms.sales.delivery.models.DeliveryResponseBody;
import com.esotacredit.esotaapi.api.cms.sales.delivery.objects.ItemDelivery;


import com.esotacredit.esotaapi.api.cms.sales.message.Message;
import com.esotacredit.esotaapi.api.cms.sales.pending.objects.Pending;
import com.esotacredit.esotaapi.api.cms.sales.sold.models.SoldBody;
import com.esotacredit.esotaapi.api.cms.sales.sold.models.SoldResponseBody;
import com.esotacredit.esotaapi.api.cms.sales.sold.objects.ItemSold;
import com.esotacredit.esotaapi.api.ecommerce.sells.datamodel.getpending.PendingBody;
import com.esotacredit.esotaapi.api.ecommerce.sells.datamodel.getpending.PendingResponseBody;
import org.springframework.stereotype.Component;

import java.util.List;

@Component
public class SalesDaoService {

    /**
     * @Delivery CRUD
     */
    DeliveryResponseBody getAllDelivery(DeliveryBody deliveryBody){
        List<ItemDelivery> deliveryList=null;
        return new DeliveryResponseBody(deliveryList);
    }

    DeliveryResponseBody saveAsDelivery(DeliveryBody deliveryBody){
        String message = null;
        return new DeliveryResponseBody(new Message(message));
    }

    DeliveryResponseBody cancelDelivery(DeliveryBody deliveryBody){
        String message = null;
        return new DeliveryResponseBody(new Message(message));
    }

    /**
     * @Pending CRUD
     */
    /*PendingResponseBody getAllPending(PendingBody pendingBody){
        List<Pending> pendingList = null;
        return new PendingResponseBody(pendingList);
    }

    PendingResponseBody deletePending(PendingBody pendingBody){
        String message = null;
        return new PendingResponseBody(new Message(message));
    }*/


    /**
     * @Sold CRUD
     */
    SoldResponseBody getAllSold(SoldBody soldBody){
        List<ItemSold>soldList = null;
        return new SoldResponseBody(soldList);
    }


}
