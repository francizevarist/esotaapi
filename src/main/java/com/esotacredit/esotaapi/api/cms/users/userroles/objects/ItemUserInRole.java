package com.esotacredit.esotaapi.api.cms.users.userroles.objects;

public class ItemUserInRole {
    private int Id;
    private String userFullName;
    private String userPhoneNumber;
    private String userCountry;

    public ItemUserInRole() {}
    public ItemUserInRole(int id, String userFullName, String userPhoneNumber, String userCountry) {
        Id = id;
        this.userFullName = userFullName;
        this.userPhoneNumber = userPhoneNumber;
        this.userCountry = userCountry;
    }

    public int getId() {
        return Id;
    }

    public void setId(int id) {
        Id = id;
    }

    public String getUserFullName() {
        return userFullName;
    }

    public void setUserFullName(String userFullName) {
        this.userFullName = userFullName;
    }

    public String getUserPhoneNumber() {
        return userPhoneNumber;
    }

    public void setUserPhoneNumber(String userPhoneNumber) {
        this.userPhoneNumber = userPhoneNumber;
    }

    public String getUserCountry() {
        return userCountry;
    }

    public void setUserCountry(String userCountry) {
        this.userCountry = userCountry;
    }
}
