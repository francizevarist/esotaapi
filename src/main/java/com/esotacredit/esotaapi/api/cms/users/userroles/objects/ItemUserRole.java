package com.esotacredit.esotaapi.api.cms.users.userroles.objects;

public class ItemUserRole {
    private int Id;
    private String userRoleTile;
    private int numberOfUser;

    public ItemUserRole() {}

    public ItemUserRole(int id, String userRoleTile, int numberOfUser) {
        Id = id;
        this.userRoleTile = userRoleTile;
        this.numberOfUser = numberOfUser;
    }

    public int getId() {
        return Id;
    }

    public void setId(int id) {
        Id = id;
    }

    public String getUserRoleTile() {
        return userRoleTile;
    }

    public void setUserRoleTile(String userRoleTile) {
        this.userRoleTile = userRoleTile;
    }

    public int getNumberOfUser() {
        return numberOfUser;
    }

    public void setNumberOfUser(int numberOfUser) {
        this.numberOfUser = numberOfUser;
    }

}
