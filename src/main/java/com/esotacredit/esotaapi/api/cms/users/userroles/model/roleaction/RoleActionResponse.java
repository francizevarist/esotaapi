package com.esotacredit.esotaapi.api.cms.users.userroles.model.roleaction;

import com.esotacredit.esotaapi.api.cms.users.message.Message;
import com.esotacredit.esotaapi.api.cms.users.userroles.objects.ItemAction;

import java.util.List;

public class RoleActionResponse {
    private Message message;
    private List<ItemAction> actionList;

    public RoleActionResponse(RoleActionResponseBody roleActionResponseBody) {
        this.message = roleActionResponseBody.getMessage();
        this.actionList = roleActionResponseBody.getActionList();
    }

    public Message getMessage() {
        return message;
    }

    public void setMessage(Message message) {
        this.message = message;
    }

    public List<ItemAction> getActionList() {
        return actionList;
    }

    public void setActionList(List<ItemAction> actionList) {
        this.actionList = actionList;
    }
}
