package com.esotacredit.esotaapi.api.cms.dashboard.datamodel.components;

import com.esotacredit.esotaapi.api.cms.dashboard.datamodel.components.objects.ItemLatestOrder;

import java.util.List;

public class LatestOrder {
    private List<ItemLatestOrder> latestOrderList;

    public LatestOrder() {}

    public LatestOrder(List<ItemLatestOrder> latestOrderList) {
        this.latestOrderList = latestOrderList;
    }

    public List<ItemLatestOrder> getLatestOrderList() {
        return latestOrderList;
    }

    public void setLatestOrderList(List<ItemLatestOrder> latestOrderList) {
        this.latestOrderList = latestOrderList;
    }
}
