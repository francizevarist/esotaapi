package com.esotacredit.esotaapi.api.cms.sales.pending.models;

import com.esotacredit.esotaapi.api.cms.sales.message.Message;
import com.esotacredit.esotaapi.api.cms.sales.pending.objects.Pending;

import java.util.List;

public class PendingResponse {
    private Message message;
    private List<Pending> pendingList;

    public PendingResponse(PendingResponseBody pendingResponseBody) {
        this.message = pendingResponseBody.getMessage();
        this.pendingList = pendingResponseBody.getPendingList();
    }

    public Message getMessage() {
        return message;
    }

    public List<Pending> getPendingList() {
        return pendingList;
    }

}
