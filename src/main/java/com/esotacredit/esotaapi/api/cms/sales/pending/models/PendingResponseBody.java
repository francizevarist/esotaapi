package com.esotacredit.esotaapi.api.cms.sales.pending.models;

import com.esotacredit.esotaapi.api.cms.sales.message.Message;
import com.esotacredit.esotaapi.api.cms.sales.pending.objects.Pending;

import java.util.List;

public class PendingResponseBody {
    private Message message;
    private List<Pending> pendingList;

    public PendingResponseBody() {}
    public PendingResponseBody(Message message, List<Pending> pendingList) {
        this.message = message;
        this.pendingList = pendingList;
    }

    public Message getMessage() {
        return message;
    }

    public void setMessage(Message message) {
        this.message = message;
    }

    public List<Pending> getPendingList() {
        return pendingList;
    }

    public void setPendingList(List<Pending> pendingList) {
        this.pendingList = pendingList;
    }

}
