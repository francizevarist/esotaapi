package com.esotacredit.esotaapi.api.cms.shops.objects;

public class ItemShop {
    private int shopId;
    private String shopName;
    private String numberOfProducts;
    private String numberOfCustomers;
    private boolean isShopActive;

    public ItemShop() {}
    public ItemShop(int shopId, String shopName, String numberOfProducts, String numberOfCustomers, boolean isShopActive) {
        this.shopId = shopId;
        this.shopName = shopName;
        this.numberOfProducts = numberOfProducts;
        this.numberOfCustomers = numberOfCustomers;
        this.isShopActive = isShopActive;
    }

    public int getShopId() {
        return shopId;
    }

    public void setShopId(int shopId) {
        this.shopId = shopId;
    }

    public String getShopName() {
        return shopName;
    }

    public void setShopName(String shopName) {
        this.shopName = shopName;
    }

    public String getNumberOfProducts() {
        return numberOfProducts;
    }

    public void setNumberOfProducts(String numberOfProducts) {
        this.numberOfProducts = numberOfProducts;
    }

    public String getNumberOfCustomers() {
        return numberOfCustomers;
    }

    public void setNumberOfCustomers(String numberOfCustomers) {
        this.numberOfCustomers = numberOfCustomers;
    }

    public boolean isShopActive() {
        return isShopActive;
    }

    public void setShopActive(boolean shopActive) {
        isShopActive = shopActive;
    }

}
