package com.esotacredit.esotaapi.api.cms.products.ItemApprove.objects;

public class ItemToApprove {
    private int itemId;
    private String itemName;
    private int categoryId;
    private String categoryName;
    private boolean isApproved;

    public ItemToApprove() {}

    public ItemToApprove(int itemId, String itemName, int categoryId, String categoryName, boolean isApproved) {
        this.itemId = itemId;
        this.itemName = itemName;
        this.categoryId = categoryId;
        this.categoryName = categoryName;
        this.isApproved = isApproved;
    }

    public int getItemId() {
        return itemId;
    }

    public void setItemId(int itemId) {
        this.itemId = itemId;
    }

    public String getItemName() {
        return itemName;
    }

    public void setItemName(String itemName) {
        this.itemName = itemName;
    }

    public int getCategoryId() {
        return categoryId;
    }

    public void setCategoryId(int categoryId) {
        this.categoryId = categoryId;
    }

    public String getCategoryName() {
        return categoryName;
    }

    public void setCategoryName(String categoryName) {
        this.categoryName = categoryName;
    }

    public boolean isApproved() {
        return isApproved;
    }

    public void setApproved(boolean approved) {
        isApproved = approved;
    }
}
