package com.esotacredit.esotaapi.api.cms.dashboard;

import com.esotacredit.esotaapi.api.cms.dashboard.datamodel.DashboardBody;
import com.esotacredit.esotaapi.api.cms.dashboard.datamodel.DashboardResponseBody;
import com.esotacredit.esotaapi.api.cms.dashboard.datamodel.components.*;
import com.esotacredit.esotaapi.api.cms.dashboard.datamodel.components.objects.ItemLatestCustomer;
import com.esotacredit.esotaapi.api.cms.dashboard.datamodel.components.objects.ItemLatestOrder;
import com.esotacredit.esotaapi.database.DatabaseConfiguration;
import com.esotacredit.esotaapi.values.Values;
import org.springframework.stereotype.Component;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

@Component
public class DashboardDaoService {

    public DashboardResponseBody getDashboard(DashboardBody dashboardBody){

        /**
         * get categories size
         * get product size
         * get customer size
         * get system user size
         * get credit list size
         * get total sells size
         */

         int mCategorySize   = Values.EMPTY_OFF_VALUE;
         int mProductSize    = Values.EMPTY_OFF_VALUE;
         int mCustomerSize   = Values.EMPTY_OFF_VALUE;
         int mUserSize       = Values.EMPTY_OFF_VALUE;
         int mCreditSize     = Values.EMPTY_OFF_VALUE;
         int mTotalSellsSize = Values.EMPTY_OFF_VALUE;
         DashboardResponseBody dashboardResponseBody = null;

        List<ItemLatestOrder> latestOrderList = new ArrayList<>();
        List<ItemLatestCustomer> latestCustomerList = new ArrayList<>();

        Connection con = DatabaseConfiguration.getDatabeConnection();
        try {

            //category count
            String queryCategory = "SELECT count(*) AS size FROM product_category";
            Statement stmt_category = con.createStatement();
            ResultSet rs_category =stmt_category.executeQuery(queryCategory);

            while(rs_category.next()){
                mCategorySize = rs_category.getInt("size");
            }

            //Product
            String queryProduct = "SELECT count(*) AS size FROM product";
            Statement stmt_product = con.createStatement();
            ResultSet rs_product =stmt_product.executeQuery(queryProduct);

            while(rs_product.next()){
                mProductSize = rs_product.getInt("size");
            }

            //Customer Size
            String queryCustomer = "SELECT user_id,COUNT(*) AS size FROM sells GROUP BY sells.user_id";
            Statement stmt_customer = con.createStatement();
            ResultSet rs_customer =stmt_customer.executeQuery(queryCustomer);

            int sumCustomer=0;
            while(rs_customer.next()){
                sumCustomer = sumCustomer+rs_customer.getInt("size");
            }
            mCustomerSize = sumCustomer;

            //User Size
            String queryUser = "SELECT COUNT(*) AS size FROM user WHERE user_status='1' AND block_status='0' AND deletion_status = '0'";
            Statement stmt_user = con.createStatement();
            ResultSet rs_user =stmt_user.executeQuery(queryUser);

            while(rs_user.next()){
                mUserSize = rs_user.getInt("size");
            }

            //Credit Size
            String queryCredit = "SELECT COUNT(*) AS size FROM network WHERE credited_id <> '0' ";
            Statement stmt_credit = con.createStatement();
            ResultSet rs_credit =stmt_credit.executeQuery(queryCredit);

            while(rs_credit.next()){
                mCreditSize = rs_credit.getInt("size");
            }

            //Total Sells Size
            String querySells = "SELECT COUNT(*) AS size FROM sells WHERE payment_status = '2' ";
            Statement stmt_sells= con.createStatement();
            ResultSet rs_sells =stmt_sells.executeQuery(querySells);

            while(rs_sells.next()){
                mTotalSellsSize = rs_sells.getInt("size");
            }



            /**
             * Latest order
             * @productName
             * @quantity
             * @amount
             * @dateAndTime
             * @userFullName
             * @phoneNumber
             */
            String queryLatestSells = "SELECT *  FROM sells WHERE payment_status = '2'";
            Statement stmt_LatestSells= con.createStatement();
            ResultSet rs_LatestSells =stmt_LatestSells.executeQuery(queryLatestSells);

            while(rs_LatestSells.next()){
                int productId = rs_LatestSells.getInt("");
                String productName = rs_LatestSells.getString("");
                int quantity = rs_LatestSells.getInt("");
                String productAmount = rs_LatestSells.getString("");
                String dateAndTime = rs_LatestSells.getString("");
                String userFullName = rs_LatestSells.getString("");
                String phoneNumber = rs_LatestSells.getString("");
                String paymentStatus = rs_LatestSells.getString("");
                String shopName = rs_LatestSells.getString("");

                ItemLatestOrder itemLatestOrder = new ItemLatestOrder();
                itemLatestOrder.setProductId(productId);
                itemLatestOrder.setProductName(productName);
                itemLatestOrder.setQuantity(quantity);
                itemLatestOrder.setAmount(productAmount);
                itemLatestOrder.setDateAndTime(dateAndTime);
                itemLatestOrder.setUserFullName(userFullName);
                itemLatestOrder.setPhoneNumber(phoneNumber);
                itemLatestOrder.setPaymentStatus(paymentStatus);
                itemLatestOrder.setShopName(shopName);

                latestOrderList.add(itemLatestOrder);
            }

            /**
             * Latest Customer
             * @customerId
             * @customerName
             * @productName
             * @quantity
             * @time
             * @paymentStatus
             */
            String queryLatestCustomer = "SELECT *  FROM sells WHERE payment_status = '2' GROUP BY user_id ";
            Statement stmt_LatestCustomer= con.createStatement();
            ResultSet rs_LatestCustomer =stmt_LatestCustomer.executeQuery(queryLatestCustomer);

            while(rs_LatestCustomer.next()){
                int productId = rs_LatestCustomer.getInt("");
                String productName = rs_LatestCustomer.getString("");
                int quantity =rs_LatestCustomer.getInt("");
                String productAmount = rs_LatestCustomer.getString("");
                String dateAndTime = rs_LatestCustomer.getString("");
                String userFullName = rs_LatestCustomer.getString("");
                String phoneNumber = rs_LatestCustomer.getString("");
                String paymentStatus = rs_LatestCustomer.getString("");
                String shopName = rs_LatestCustomer.getString("");
                latestCustomerList.add(new ItemLatestCustomer(productId,productName,quantity,productAmount,dateAndTime,userFullName,phoneNumber,paymentStatus,shopName));
            }


            /**
             * Graph data
             */

            dashboardResponseBody = new DashboardResponseBody(
                    new Categories(mCategorySize),
                    new Credits(mCreditSize),
                    new Customers(mCustomerSize),
                    new Products(mProductSize),
                    new TotalSells(mTotalSellsSize),
                    new Users(mUserSize),
                    new LatestOrder(latestOrderList),
                    new LatestCustomer(latestCustomerList)
            );

            con.close();
        } catch (SQLException e) {
            e.printStackTrace();
        }


        return dashboardResponseBody;
    }



}
