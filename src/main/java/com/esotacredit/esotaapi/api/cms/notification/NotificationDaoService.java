package com.esotacredit.esotaapi.api.cms.notification;

import com.esotacredit.esotaapi.api.cms.notification.model.NotificationBody;
import com.esotacredit.esotaapi.api.cms.notification.model.NotificationResponseBody;
import com.esotacredit.esotaapi.api.cms.notification.objects.QuickInfo;
import org.springframework.stereotype.Component;

@Component
public class NotificationDaoService {

    public NotificationResponseBody getQuickInfo(NotificationBody notificationBody){
        QuickInfo quickInfo = null;
        return new NotificationResponseBody(quickInfo);
    }

}
