package com.esotacredit.esotaapi.api.cms.products;

import com.esotacredit.esotaapi.api.cms.products.categories.CategoryBody;
import com.esotacredit.esotaapi.api.cms.products.categories.CategoryResponseBody;
import com.esotacredit.esotaapi.api.cms.products.categories.objects.ItemCategory;
import com.esotacredit.esotaapi.api.cms.products.categories.objects.Message;
import com.esotacredit.esotaapi.database.DatabaseConfiguration;
import org.springframework.stereotype.Component;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.List;

@Component
public class ProductsDaoService {

    /**
     * Category CRUD
     */
    CategoryResponseBody getAllCategories(CategoryBody categoryBody){
        List<ItemCategory> categoryList = null;

        try{
            Connection con = DatabaseConfiguration.getDatabeConnection();
            //category count
            String queryGetCategory = "";
            Statement stmt_getCategory = con.createStatement();
            ResultSet rs_getCategory =stmt_getCategory.executeQuery(queryGetCategory);

            while(rs_getCategory.next()){
            }

            con.close();

        } catch (
        SQLException e) {
            e.printStackTrace();
        }

        return new CategoryResponseBody(categoryList);
    }

    CategoryResponseBody addCategory(CategoryBody categoryBody){
        String message=null;

        try{
            Connection con = DatabaseConfiguration.getDatabeConnection();
            //category count
            String queryAddCategory = "";
            Statement stmt_addCategory = con.createStatement();
            ResultSet rs_addCategory =stmt_addCategory.executeQuery(queryAddCategory);

            while(rs_addCategory.next()){

            }

            con.close();
        } catch (
                SQLException e) {
            e.printStackTrace();
        }

        return new CategoryResponseBody(new Message(message));
    }

    CategoryResponseBody deleteCategory(CategoryBody categoryBody){
        String message = null;

        try{
            Connection con = DatabaseConfiguration.getDatabeConnection();
            //category count
            String queryDeleteCategory = "";
            Statement stmt_deleteCategory = con.createStatement();
            ResultSet rs_deleteCategory =stmt_deleteCategory.executeQuery(queryDeleteCategory);

            while(rs_deleteCategory.next()){
            }

            con.close();
        } catch (
                SQLException e) {
            e.printStackTrace();
        }

        return new CategoryResponseBody(new Message(message));
    }

    CategoryResponseBody editCategory(CategoryBody categoryBody){
        String message = null;

        try{
            Connection con = DatabaseConfiguration.getDatabeConnection();
            //category count
            String queryEditCategory = "";
            Statement stmt_editCategory = con.createStatement();
            ResultSet rs_editCategory =stmt_editCategory.executeQuery(queryEditCategory);

            while(rs_editCategory.next()){
            }

            con.close();
        } catch (
                SQLException e) {
            e.printStackTrace();
        }

        return new CategoryResponseBody(new Message(message));
    }

}
