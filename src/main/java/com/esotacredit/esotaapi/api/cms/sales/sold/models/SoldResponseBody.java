package com.esotacredit.esotaapi.api.cms.sales.sold.models;

import com.esotacredit.esotaapi.api.cms.sales.message.Message;
import com.esotacredit.esotaapi.api.cms.sales.sold.objects.ItemSold;

import java.util.List;

public class SoldResponseBody {
    private Message message;
    private List<ItemSold> soldList;

    public SoldResponseBody() {}

    public SoldResponseBody(Message message) {
        this.message = message;
    }

    public SoldResponseBody(List<ItemSold> soldList) {
        this.soldList = soldList;
    }

    public SoldResponseBody(Message message, List<ItemSold> soldList) {
        this.message = message;
        this.soldList = soldList;
    }

    public Message getMessage() {
        return message;
    }

    public void setMessage(Message message) {
        this.message = message;
    }

    public List<ItemSold> getSoldList() {
        return soldList;
    }

    public void setSoldList(List<ItemSold> soldList) {
        this.soldList = soldList;
    }
}
