package com.esotacredit.esotaapi.api.cms.users.systemuser.objects;

public class ItemUser {
    private int userId;
    private String userName;
    private String phoneNumber;
    private String country;
    private int totalPuschase;
    private String totalCredits;
    private boolean isActive;

    public ItemUser() {}
    public ItemUser(int userId, String userName, String phoneNumber, String country, int totalPuschase, String totalCredits, boolean isActive) {
        this.userId = userId;
        this.userName = userName;
        this.phoneNumber = phoneNumber;
        this.country = country;
        this.totalPuschase = totalPuschase;
        this.totalCredits = totalCredits;
        this.isActive = isActive;
    }

    public int getUserId() {
        return userId;
    }

    public void setUserId(int userId) {
        this.userId = userId;
    }

    public String getUserName() {
        return userName;
    }

    public void setUserName(String userName) {
        this.userName = userName;
    }

    public String getPhoneNumber() {
        return phoneNumber;
    }

    public void setPhoneNumber(String phoneNumber) {
        this.phoneNumber = phoneNumber;
    }

    public String getCountry() {
        return country;
    }

    public void setCountry(String country) {
        this.country = country;
    }

    public int getTotalPuschase() {
        return totalPuschase;
    }

    public void setTotalPuschase(int totalPuschase) {
        this.totalPuschase = totalPuschase;
    }

    public String getTotalCredits() {
        return totalCredits;
    }

    public void setTotalCredits(String totalCredits) {
        this.totalCredits = totalCredits;
    }

    public boolean isActive() {
        return isActive;
    }

    public void setActive(boolean active) {
        isActive = active;
    }

}
