package com.esotacredit.esotaapi.api.cms.notification.objects;

public class QuickInfo {
    private int loggedIn;
    private int totalSalesToday;
    private int newOrdersToday;

    public QuickInfo(int loggedIn, int totalSalesToday, int newOrdersToday) {
        this.loggedIn = loggedIn;
        this.totalSalesToday = totalSalesToday;
        this.newOrdersToday = newOrdersToday;
    }

    public int getLoggedIn() {
        return loggedIn;
    }

    public void setLoggedIn(int loggedIn) {
        this.loggedIn = loggedIn;
    }

    public int getTotalSalesToday() {
        return totalSalesToday;
    }

    public void setTotalSalesToday(int totalSalesToday) {
        this.totalSalesToday = totalSalesToday;
    }

    public int getNewOrdersToday() {
        return newOrdersToday;
    }

    public void setNewOrdersToday(int newOrdersToday) {
        this.newOrdersToday = newOrdersToday;
    }

}
