package com.esotacredit.esotaapi.api.cms.dashboard.datamodel;

import com.esotacredit.esotaapi.api.cms.dashboard.datamodel.components.*;

public class DashboardResponseBody {
    private Categories categorySize;
    private Credits creditSize;
    private Customers customerSize;
    private Products productSize;
    private TotalSells totalSellsSize;
    private Users userSize;
    private LatestOrder latestOrder;
    private LatestCustomer latestCustomer;

    public DashboardResponseBody() {}
    public DashboardResponseBody(Categories categorySize,
                                 Credits creditSize,
                                 Customers customerSize,
                                 Products productSize,
                                 TotalSells totalSellsSize,
                                 Users userSize,
                                 LatestOrder latestOrder,
                                 LatestCustomer latestCustomer) {
        this.categorySize = categorySize;
        this.creditSize = creditSize;
        this.customerSize = customerSize;
        this.productSize = productSize;
        this.totalSellsSize = totalSellsSize;
        this.userSize = userSize;
        this.latestOrder = latestOrder;
        this.latestCustomer = latestCustomer;
    }

    public Categories getCategorySize() {
        return categorySize;
    }

    public void setCategorySize(Categories categorySize) {
        this.categorySize = categorySize;
    }

    public Credits getCreditSize() {
        return creditSize;
    }

    public void setCreditSize(Credits creditSize) {
        this.creditSize = creditSize;
    }

    public Customers getCustomerSize() {
        return customerSize;
    }

    public void setCustomerSize(Customers customerSize) {
        this.customerSize = customerSize;
    }

    public Products getProductSize() {
        return productSize;
    }

    public void setProductSize(Products productSize) {
        this.productSize = productSize;
    }

    public TotalSells getTotalSellsSize() {
        return totalSellsSize;
    }

    public void setTotalSellsSize(TotalSells totalSellsSize) {
        this.totalSellsSize = totalSellsSize;
    }

    public Users getUserSize() {
        return userSize;
    }

    public void setUserSize(Users userSize) {
        this.userSize = userSize;
    }

    public LatestOrder getLatestOrder() {
        return latestOrder;
    }

    public void setLatestOrder(LatestOrder latestOrder) {
        this.latestOrder = latestOrder;
    }

    public LatestCustomer getLatestCustomer() {
        return latestCustomer;
    }

    public void setLatestCustomer(LatestCustomer latestCustomer) {
        this.latestCustomer = latestCustomer;
    }

}
