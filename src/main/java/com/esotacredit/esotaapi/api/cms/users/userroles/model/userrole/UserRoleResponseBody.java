package com.esotacredit.esotaapi.api.cms.users.userroles.model.userrole;

import com.esotacredit.esotaapi.api.cms.users.message.Message;
import com.esotacredit.esotaapi.api.cms.users.userroles.objects.ItemUserRole;

import java.util.List;

public class UserRoleResponseBody {
    private Message message;
    private List<ItemUserRole> userRoleList;

    public UserRoleResponseBody() {}

    public UserRoleResponseBody(Message message, List<ItemUserRole> userRoleList) {
        this.message = message;
        this.userRoleList = userRoleList;
    }

    public Message getMessage() {
        return message;
    }

    public void setMessage(Message message) {
        this.message = message;
    }

    public List<ItemUserRole> getUserRoleList() {
        return userRoleList;
    }

    public void setUserRoleList(List<ItemUserRole> userRoleList) {
        this.userRoleList = userRoleList;
    }

}
