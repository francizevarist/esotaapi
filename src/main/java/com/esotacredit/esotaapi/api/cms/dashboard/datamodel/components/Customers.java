package com.esotacredit.esotaapi.api.cms.dashboard.datamodel.components;

public class Customers {
    private int customerSize;

    public Customers() {}

    public Customers(int customerSize) {
        this.customerSize = customerSize;
    }

    public int getCustomerSize() {
        return customerSize;
    }
}
