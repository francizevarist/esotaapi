package com.esotacredit.esotaapi.api.cms.shops;

import com.esotacredit.esotaapi.api.cms.shops.model.ShopBody;
import com.esotacredit.esotaapi.api.cms.shops.model.ShopResponse;
import com.esotacredit.esotaapi.api.cms.shops.objects.ItemShop;
import com.esotacredit.esotaapi.api.cms.shops.objects.Message;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@CrossOrigin(origins = "*", allowedHeaders = "*")
@RestController
public class ShopResource {

    @Autowired
    ShopDaoService service;

    @PostMapping("/allshops")
    public List<ItemShop> getAllShop(@RequestBody ShopBody shopBody){
        return new ShopResponse(service.getAllShops(shopBody)).getShopList();
    }

    @PostMapping("/addshops")
    public Message addShop(@RequestBody ShopBody shopBody){
        return new ShopResponse(service.addShop(shopBody)).getMessage();
    }

    @PostMapping("/deleteshops")
    public Message deleteShop(@RequestBody ShopBody shopBody){
        return new ShopResponse(service.deleteShop(shopBody)).getMessage();
    }

    @PostMapping("/editshops")
    public Message editShop(@RequestBody ShopBody shopBody){
        return new ShopResponse(service.editShop(shopBody)).getMessage();
    }

}
