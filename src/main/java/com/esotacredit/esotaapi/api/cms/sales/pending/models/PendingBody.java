package com.esotacredit.esotaapi.api.cms.sales.pending.models;

public class PendingBody {
    private int userId;

    public PendingBody() {}

    public PendingBody(int userId) {
        this.userId = userId;
    }

    public int getUserId() {
        return userId;
    }

    public void setUserId(int userId) {
        this.userId = userId;
    }

}
