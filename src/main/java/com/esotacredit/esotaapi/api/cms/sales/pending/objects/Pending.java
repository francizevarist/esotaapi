package com.esotacredit.esotaapi.api.cms.sales.pending.objects;

public class Pending {
    private int productId;
    private String productName;
    private String purchasedTime;
    private String referenceNumber;
    private boolean paymentStatus;
    private int userId;
    private String userName;

    public Pending() {}
    public Pending(int productId, String productName, String purchasedTime, String referenceNumber, boolean paymentStatus, int userId, String userName) {
        this.productId = productId;
        this.productName = productName;
        this.purchasedTime = purchasedTime;
        this.referenceNumber = referenceNumber;
        this.paymentStatus = paymentStatus;
        this.userId = userId;
        this.userName = userName;
    }

    public int getProductId() {
        return productId;
    }

    public void setProductId(int productId) {
        this.productId = productId;
    }

    public String getProductName() {
        return productName;
    }

    public void setProductName(String productName) {
        this.productName = productName;
    }

    public String getPurchasedTime() {
        return purchasedTime;
    }

    public void setPurchasedTime(String purchasedTime) {
        this.purchasedTime = purchasedTime;
    }

    public String getReferenceNumber() {
        return referenceNumber;
    }

    public void setReferenceNumber(String referenceNumber) {
        this.referenceNumber = referenceNumber;
    }

    public boolean isPaymentStatus() {
        return paymentStatus;
    }

    public void setPaymentStatus(boolean paymentStatus) {
        this.paymentStatus = paymentStatus;
    }

    public int getUserId() {
        return userId;
    }

    public void setUserId(int userId) {
        this.userId = userId;
    }

    public String getUserName() {
        return userName;
    }

    public void setUserName(String userName) {
        this.userName = userName;
    }

}
