package com.esotacredit.esotaapi.api.cms.products.categories;

public class CategoryBody {
    private int userId;

    //add attributes
    private String name;
    private String tagline;

    public CategoryBody() {}

    //getAll | Categories | delete
    public CategoryBody(int userId) {
        this.userId = userId;
    }

    //add | edit
    public CategoryBody(int userId, String name, String tagline) {
        this.userId = userId;
        this.name = name;
        this.tagline = tagline;
    }

    public int getUserId() {
        return userId;
    }

    public String getName() {
        return name;
    }

    public String getTagline() {
        return tagline;
    }

}
