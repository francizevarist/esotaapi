package com.esotacredit.esotaapi.api.cms.sales.sold.models;

import com.esotacredit.esotaapi.api.cms.sales.message.Message;
import com.esotacredit.esotaapi.api.cms.sales.sold.objects.ItemSold;

import java.util.List;

public class SoldResponse {
    private Message message;
    private List<ItemSold> soldList;

    public SoldResponse(SoldResponseBody soldResponseBody) {
        this.message = soldResponseBody.getMessage();
        this.soldList = soldResponseBody.getSoldList();
    }

    public Message getMessage() {
        return message;
    }

    public void setMessage(Message message) {
        this.message = message;
    }

    public List<ItemSold> getSoldList() {
        return soldList;
    }

    public void setSoldList(List<ItemSold> soldList) {
        this.soldList = soldList;
    }

}
