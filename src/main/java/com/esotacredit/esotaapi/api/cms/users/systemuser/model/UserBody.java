package com.esotacredit.esotaapi.api.cms.users.systemuser.model;

public class UserBody {
    private int userId;

    public UserBody() {}

    public UserBody(int userId) {
        this.userId = userId;
    }

    public int getUserId() {
        return userId;
    }

    public void setUserId(int userId) {
        this.userId = userId;
    }
}
