package com.esotacredit.esotaapi.api.cms.products.items.objects;

public class ItemProductItem {
    private int itemId;
    private String itemName;
    private int categoryId;
    private String categoryName;
    private int shopId;
    private String shopName;
    private boolean active;

    public ItemProductItem() {}
    public ItemProductItem(int itemId, String itemName, int categoryId, String categoryName, int shopId, String shopName, boolean active) {
        this.itemId = itemId;
        this.itemName = itemName;
        this.categoryId = categoryId;
        this.categoryName = categoryName;
        this.shopId = shopId;
        this.shopName = shopName;
        this.active = active;
    }

    public int getItemId() {
        return itemId;
    }

    public void setItemId(int itemId) {
        this.itemId = itemId;
    }

    public String getItemName() {
        return itemName;
    }

    public void setItemName(String itemName) {
        this.itemName = itemName;
    }

    public int getCategoryId() {
        return categoryId;
    }

    public void setCategoryId(int categoryId) {
        this.categoryId = categoryId;
    }

    public String getCategoryName() {
        return categoryName;
    }

    public void setCategoryName(String categoryName) {
        this.categoryName = categoryName;
    }

    public int getShopId() {
        return shopId;
    }

    public void setShopId(int shopId) {
        this.shopId = shopId;
    }

    public String getShopName() {
        return shopName;
    }

    public void setShopName(String shopName) {
        this.shopName = shopName;
    }

    public boolean isActive() {
        return active;
    }

    public void setActive(boolean active) {
        this.active = active;
    }

}
