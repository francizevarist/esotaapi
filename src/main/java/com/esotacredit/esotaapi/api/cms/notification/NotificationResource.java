package com.esotacredit.esotaapi.api.cms.notification;

import com.esotacredit.esotaapi.api.cms.notification.model.NotificationBody;
import com.esotacredit.esotaapi.api.cms.notification.model.NotificationResponse;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

@CrossOrigin(origins = "*", allowedHeaders = "*")
@RestController
public class NotificationResource {

    @Autowired
    NotificationDaoService service;

    @PostMapping("/quicknotification")
    public NotificationResponse getNotification(@RequestBody NotificationBody notificationBody){
        return new NotificationResponse(service.getQuickInfo(notificationBody));
    }


}
