package com.esotacredit.esotaapi.api.cms.shops.model;

import com.esotacredit.esotaapi.api.cms.shops.objects.ItemShop;
import com.esotacredit.esotaapi.api.cms.shops.objects.Message;

import java.util.List;

public class ShopResponseBody {
    private Message message;
    private List<ItemShop> shopList;

    public ShopResponseBody(Message message) {
        this.message = message;
    }

    public ShopResponseBody(List<ItemShop> shopList) {
        this.shopList = shopList;
    }

    public ShopResponseBody(Message message, List<ItemShop> shopList) {
        this.message = message;
        this.shopList = shopList;
    }

    public Message getMessage() {
        return message;
    }

    public List<ItemShop> getShopList() {
        return shopList;
    }
}
