package com.esotacredit.esotaapi.api.cms.users.systemuser.model;

import com.esotacredit.esotaapi.api.cms.users.systemuser.objects.ItemUser;
import com.esotacredit.esotaapi.api.cms.users.message.Message;

import java.util.List;

public class UserResponse {
    private Message message;
    private List<ItemUser> userList;

    public UserResponse(UserResponseBody userResponseBody) {
        this.message = userResponseBody.getMessage();
        this.userList = userResponseBody.getUserList();
    }

    public Message getMessage() {
        return message;
    }

    public List<ItemUser> getUserList() {
        return userList;
    }
}
