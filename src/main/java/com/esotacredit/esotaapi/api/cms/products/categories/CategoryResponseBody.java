package com.esotacredit.esotaapi.api.cms.products.categories;

import com.esotacredit.esotaapi.api.cms.products.categories.objects.ItemCategory;
import com.esotacredit.esotaapi.api.cms.products.categories.objects.Message;

import java.util.List;

public class CategoryResponseBody {

    Message message;
    List<ItemCategory> categoryList;

    public CategoryResponseBody() {}

    public CategoryResponseBody(Message message, List<ItemCategory> categoryList) {
        this.message = message;
        this.categoryList = categoryList;
    }

    public CategoryResponseBody(Message message) {
        this.message = message;
    }

    public CategoryResponseBody(List<ItemCategory> categoryList) {
        this.categoryList = categoryList;
    }

    public Message getMessage() {
        return message;
    }

    public void setMessage(Message message) {
        this.message = message;
    }

    public List<ItemCategory> getCategoryList() {
        return categoryList;
    }

    public void setCategoryList(List<ItemCategory> categoryList) {
        this.categoryList = categoryList;
    }
}
