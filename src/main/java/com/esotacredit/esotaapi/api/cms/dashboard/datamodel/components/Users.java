package com.esotacredit.esotaapi.api.cms.dashboard.datamodel.components;

public class Users {
    private int userSize;

    public Users() {}
    public Users(int userSize) {
        this.userSize = userSize;
    }

    public int getUserSize() {
        return userSize;
    }
}
