package com.esotacredit.esotaapi.api.cms.products;

import com.esotacredit.esotaapi.api.cms.products.categories.CategoryBody;
import com.esotacredit.esotaapi.api.cms.products.categories.CategoryResponse;
import com.esotacredit.esotaapi.api.cms.products.categories.CategoryResponseBody;
import com.esotacredit.esotaapi.api.cms.products.categories.objects.ItemCategory;
import com.esotacredit.esotaapi.api.cms.products.categories.objects.Message;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@CrossOrigin(origins = "*", allowedHeaders = "*")
@RestController
public class ProductsResource {

    @Autowired
    ProductsDaoService service;

    /**
     * @category CRUD
     */
    @PostMapping("getcategories")
    List<ItemCategory> getAllCategory(@RequestBody CategoryBody categoryBody){
        return new CategoryResponse(service.getAllCategories(categoryBody)).getCategoryList();
    }

    @PostMapping("addcategory")
    Message addCategory(@RequestBody CategoryBody categoryBody){
        return new CategoryResponse(service.addCategory(categoryBody)).getMessage();
    }

    @PostMapping("deletecategory")
    Message deleteCategory(@RequestBody CategoryBody categoryBody){
        return new CategoryResponse(service.deleteCategory(categoryBody)).getMessage();
    }

    @PostMapping("editcategory")
    Message editCategory(@RequestBody CategoryBody categoryBody){
        return new CategoryResponse(service.editCategory(categoryBody)).getMessage();
    }

}
