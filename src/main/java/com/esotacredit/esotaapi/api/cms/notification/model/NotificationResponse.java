package com.esotacredit.esotaapi.api.cms.notification.model;

import com.esotacredit.esotaapi.api.cms.notification.objects.QuickInfo;

public class NotificationResponse {
    private QuickInfo quickInfo;

    public NotificationResponse(NotificationResponseBody notificationResponseBody) {
        this.quickInfo = notificationResponseBody.getQuickInfo();
    }
}
