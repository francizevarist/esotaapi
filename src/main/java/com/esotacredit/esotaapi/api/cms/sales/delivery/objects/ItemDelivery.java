package com.esotacredit.esotaapi.api.cms.sales.delivery.objects;

public class ItemDelivery {
    private int productId;
    private String productName;
    private int userId;
    private String userFullName;
    private String deliveryLocation;
    private String deliveryAddress;
    private String totalAmount;
    private boolean paymentStatus;

    public ItemDelivery() {}
    public ItemDelivery(int productId, String productName, int userId, String userFullName, String deliveryLocation, String deliveryAddress, String totalAmount, boolean paymentStatus) {
        this.productId = productId;
        this.productName = productName;
        this.userId = userId;
        this.userFullName = userFullName;
        this.deliveryLocation = deliveryLocation;
        this.deliveryAddress = deliveryAddress;
        this.totalAmount = totalAmount;
        this.paymentStatus = paymentStatus;
    }
}
