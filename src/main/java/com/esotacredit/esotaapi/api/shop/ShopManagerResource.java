package com.esotacredit.esotaapi.api.shop;


import com.esotacredit.esotaapi.api.shop.model.productlist.AddProductBody;
import com.esotacredit.esotaapi.api.shop.model.productlist.ProductListBody;
import com.esotacredit.esotaapi.api.shop.model.productlist.ProductListResponse;
import com.esotacredit.esotaapi.api.shop.model.productlist.ProductListResponseBody;
import com.esotacredit.esotaapi.api.shop.objects.ItemShopProduct;
import com.esotacredit.esotaapi.api.shop.objects.ItemSoldProduct;
import com.esotacredit.esotaapi.api.shop.objects.Message;
import com.esotacredit.esotaapi.api.shop.objects.ProductImageData;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.configurationprocessor.json.JSONArray;
import org.springframework.boot.configurationprocessor.json.JSONException;
import org.springframework.boot.configurationprocessor.json.JSONObject;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import java.util.ArrayList;
import java.util.List;

@CrossOrigin(origins = "*", allowedHeaders = "*")
@RestController
public class ShopManagerResource {

    @Autowired
    ShopManagerDaoService service;

    /**
     * Product list ,edit ,delete
     */

    @PostMapping("/shopsoldproducts")
    List<ItemSoldProduct> getSoldProduct(@RequestBody ProductListBody productListBody){
        return new ProductListResponse(service.getSoldProduct(productListBody)).getSoldProductList();
    }

    @PostMapping("/shopproducts")
    List<ItemShopProduct> getAllProduct(@RequestBody ProductListBody productListBody){
        return new ProductListResponse(service.getAllProduct(productListBody)).getProductList();
    }

    @PostMapping("/shopdeleteproduct")
    Message deleteProduct(@RequestBody ProductListBody productListBody){
        return new ProductListResponse(service.deleteProduct(productListBody)).getMessage();
    }

    @PostMapping("/shopaddproduct")
    Message addProduct(@RequestBody String json){
        AddProductBody addProductBody = new AddProductBody();
        List<ProductImageData> productImageDataList = new ArrayList<>();
        try {
            JSONObject obj = new JSONObject(json);
            addProductBody.setProductName(obj.getString("productName"));
            addProductBody.setShopId(obj.getInt("shopId"));
            addProductBody.setQuantity(obj.getInt("quantity"));
            addProductBody.setSellingPrice(obj.getString("sellingPrice"));
            addProductBody.setCategoryId(obj.getInt("categoryId"));
            addProductBody.setDescription(obj.getString("description"));
            addProductBody.setCountryId(obj.getInt("countryId"));
            JSONArray arr = obj.getJSONArray("productImageDataList");
            for (int i = 0; i < arr.length(); i++) {
                ProductImageData productImageData = new ProductImageData(arr.getJSONObject(i).getString("productImageData"));
                productImageDataList.add(productImageData);
            }
            addProductBody.setProductImageDataList(productImageDataList);
        } catch (JSONException e) {
            e.printStackTrace();
        }

        return new ProductListResponse(service.addProduct(addProductBody)).getMessage();
    }

    @PostMapping("/shopeditproduct")
    Message editProduct(@RequestBody AddProductBody addProductBody ){
        return new ProductListResponse(service.editProduct(addProductBody)).getMessage();
    }

}
