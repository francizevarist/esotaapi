package com.esotacredit.esotaapi.api.shop.model.productlist;

public class ProductListBody {
    private int shopId;
    private int productId;
    private int quantity;
    private String SellingPrice;



    public ProductListBody() {}

    //@data for viewlist
    public ProductListBody(int shopId) {
        this.shopId = shopId;
    }

    //@data for delete
    public ProductListBody(int shopId, int productId) {
        this.shopId = shopId;
        this.productId = productId;
    }

    //@data for edit(update)
    public ProductListBody(int shopId, int productId, int quantity, String sellingPrice) {
        this.shopId = shopId;
        this.productId = productId;
        this.quantity = quantity;
        SellingPrice = sellingPrice;
    }

    public int getShopId() {
        return shopId;
    }

    public void setShopId(int shopId) {
        this.shopId = shopId;
    }

    public int getProductId() {
        return productId;
    }

    public void setProductId(int productId) {
        this.productId = productId;
    }

    public int getQuantity() {
        return quantity;
    }

    public void setQuantity(int quantity) {
        this.quantity = quantity;
    }

    public String getSellingPrice() {
        return SellingPrice;
    }

    public void setSellingPrice(String sellingPrice) {
        SellingPrice = sellingPrice;
    }
}
