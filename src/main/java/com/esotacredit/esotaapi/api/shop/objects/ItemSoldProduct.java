package com.esotacredit.esotaapi.api.shop.objects;

public class ItemSoldProduct {
    private int Id;
    private String productName;
    private String sellingPrice;
    private String referenceNumber;
    private int itemSold;
    private int availableStock;
    private String paymentTime;

    public ItemSoldProduct() {}

    public ItemSoldProduct(int id, String productName, String sellingPrice, String referenceNumber, int itemSold, int availableStock, String paymentTime) {
        Id = id;
        this.productName = productName;
        this.sellingPrice = sellingPrice;
        this.referenceNumber = referenceNumber;
        this.itemSold = itemSold;
        this.availableStock = availableStock;
        this.paymentTime = paymentTime;
    }

    public int getId() {
        return Id;
    }

    public void setId(int id) {
        Id = id;
    }

    public String getProductName() {
        return productName;
    }

    public void setProductName(String productName) {
        this.productName = productName;
    }

    public String getSellingPrice() {
        return sellingPrice;
    }

    public void setSellingPrice(String sellingPrice) {
        this.sellingPrice = sellingPrice;
    }

    public String getReferenceNumber() {
        return referenceNumber;
    }

    public void setReferenceNumber(String referenceNumber) {
        this.referenceNumber = referenceNumber;
    }

    public int getItemSold() {
        return itemSold;
    }

    public void setItemSold(int itemSold) {
        this.itemSold = itemSold;
    }

    public int getAvailableStock() {
        return availableStock;
    }

    public void setAvailableStock(int availableStock) {
        this.availableStock = availableStock;
    }

    public String getPaymentTime() {
        return paymentTime;
    }

    public void setPaymentTime(String paymentTime) {
        this.paymentTime = paymentTime;
    }
}
