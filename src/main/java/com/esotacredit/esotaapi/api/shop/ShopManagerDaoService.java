package com.esotacredit.esotaapi.api.shop;

import com.esotacredit.esotaapi.api.ecommerce.delivery.datamodel.deletedelivery.objects.DeleteDeliveryResponseMessage;
import com.esotacredit.esotaapi.api.shop.model.productlist.AddProductBody;
import com.esotacredit.esotaapi.api.shop.model.productlist.ProductListBody;
import com.esotacredit.esotaapi.api.shop.model.productlist.ProductListResponseBody;
import com.esotacredit.esotaapi.api.shop.objects.ItemShopProduct;
import com.esotacredit.esotaapi.api.shop.objects.ItemSoldProduct;
import com.esotacredit.esotaapi.api.shop.objects.Message;
import com.esotacredit.esotaapi.api.shop.objects.ShopImage;
import com.esotacredit.esotaapi.database.DatabaseConfiguration;
import com.esotacredit.esotaapi.utils.AppTimestamp;
import com.esotacredit.esotaapi.utils.ImageManager;
import com.esotacredit.esotaapi.utils.RandomGenerator;
import com.esotacredit.esotaapi.utils.TransactionIdGenerator;
import com.esotacredit.esotaapi.values.Values;
import org.springframework.stereotype.Component;

import javax.imageio.ImageIO;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;
import java.sql.*;
import java.util.ArrayList;
import java.util.List;

@Component
public class ShopManagerDaoService {

    public ProductListResponseBody getSoldProduct(ProductListBody productListBody){
        List<ItemSoldProduct> soldProductList = new ArrayList<>();

        Connection con = DatabaseConfiguration.getDatabeConnection();
        try {
            //get user
            String query_sold = "SELECT *,sells.id as sells_id , COUNT(sells.product_id) AS item_sold FROM products\n" +
                    "INNER JOIN sells " +
                    "ON sells.product_id = products.id " +
                    "WHERE products.shop_id='"+productListBody.getShopId()+"' AND sells.payment_status = '2' " +
                    "GROUP BY products.id";

            Statement stmt_products = con.createStatement();
            ResultSet rs_products =stmt_products.executeQuery(query_sold);
            while(rs_products.next()){
                ItemSoldProduct item = new ItemSoldProduct();
                item.setId(rs_products.getInt("sells_id"));
                item.setProductName(rs_products.getString("product_name"));
                item.setSellingPrice(rs_products.getString("selling_price"));
                item.setReferenceNumber(rs_products.getString("reference_number"));
                item.setItemSold(rs_products.getInt("item_sold"));
                item.setAvailableStock(rs_products.getInt("quantity"));
                item.setPaymentTime(rs_products.getString("payment_time"));
                soldProductList.add(item);
            }

            /**
             * Close
             */
            con.close();
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return new ProductListResponseBody(soldProductList,0);
    }

    public ProductListResponseBody getAllProduct(ProductListBody productListBody){
        List<ItemShopProduct> productList = new ArrayList<>();

        Connection con = DatabaseConfiguration.getDatabeConnection();
        try {
            //get user
            String query_products = "SELECT " +
                    "products.id," +
                    "products.product_name," +
                    "product_category.category_name," +
                    "products.quantity," +
                    "products.selling_price," +
                    "products.description," +
                    "products.approval_status " +
                    "FROM products,product_category " +
                    "WHERE products.category_id=product_category.id AND products.shop_id = '"+productListBody.getShopId()+"'";

            Statement stmt_products = con.createStatement();
            ResultSet rs_products =stmt_products.executeQuery(query_products);

            while(rs_products.next()){

                List<ShopImage> imagesList = new ArrayList<>();
                //get user

                String query_getImages = "SELECT * FROM products_images WHERE product_id='"+rs_products.getInt("id")+"'";
                Statement stmt_image = con.createStatement();
                ResultSet rs_image =stmt_image.executeQuery(query_getImages);

                while(rs_image.next()){
                    ShopImage images = new ShopImage(rs_image.getInt("id"),Values.SERVER_ADDRESS+rs_image.getString("image_url")+Values.IMAGE_FILE_FORMAT);
                    imagesList.add(images);
                }


                ItemShopProduct item = new ItemShopProduct();
                item.setId(rs_products.getInt("id"));
                item.setProductName(rs_products.getString("product_name"));
                item.setCategoryName(rs_products.getString("category_name"));
                item.setSellingPrice(rs_products.getString("selling_price"));
                item.setStockSize(rs_products.getInt("quantity"));
                item.setDescription(rs_products.getString("description"));
                item.setApprovalStatus((rs_products.getInt("approval_status")==1)?true:false);
                item.setShopImage(imagesList);
                productList.add(item);
            }

            /**
             * Close
             */
            con.close();
        } catch (SQLException e) {
            e.printStackTrace();
        }

        return new ProductListResponseBody(productList);
    }

    public ProductListResponseBody deleteProduct(ProductListBody productListBody){
        String message = null;

        Connection con = DatabaseConfiguration.getDatabeConnection();
        try {
            //get user
            String query_deleteDeliveryItem = "DELETE FROM products WHERE products.id ='"+productListBody.getProductId()+"' AND  products.shop_id = '"+productListBody.getShopId()+"'";

            Statement stmt_deleteCartItem = con.createStatement();
            stmt_deleteCartItem.execute(query_deleteDeliveryItem);
            message = "product deleted";

            /**
             * Close
             */
            con.close();
        } catch (SQLException e) {
            message = "Fail to delete";
            e.printStackTrace();
        }

        return new ProductListResponseBody(new Message(message));
    }

    ProductListResponseBody  addProduct(AddProductBody addProductBody){
        System.out.println(addProductBody.getProductImageDataList());
        String message = null;

        Connection con = DatabaseConfiguration.getDatabeConnection();
        try {
            String query_createuser = "INSERT INTO products " +
                    "(id,product_name,shop_id,quantity,selling_price,entrydate,category_id,description,approval_status) \n" +
                    "VALUES (?,?,?,?,?,?,?,?,?)";

            //save trip
            PreparedStatement preparedStatement = con.prepareStatement(query_createuser);

            preparedStatement.setInt(1, Values.EMPTY_OFF_VALUE);
            preparedStatement.setString(2, addProductBody.getProductName());
            preparedStatement.setInt(3, addProductBody.getShopId());
            preparedStatement.setInt(4, addProductBody.getQuantity());
            preparedStatement.setString(5, addProductBody.getSellingPrice());
            preparedStatement.setString(6, String.valueOf(new AppTimestamp().currentTimeStamp()));
            preparedStatement.setInt(7, addProductBody.getCategoryId());
            preparedStatement.setString(8, addProductBody.getDescription());
            preparedStatement.setInt(9, Values.EMPTY_OFF_VALUE);
            preparedStatement.execute();
            ResultSet resultSet = preparedStatement.getGeneratedKeys();

            //Inser Image to database
            resultSet.next();
            int insertedProductId = resultSet.getInt(1);
            for(int i=0; i<addProductBody.getProductImageDataList().size(); i++){
                /*
                data:image/jpeg;base64,
                 */
                //insert data to product Image
                String imageName = String.valueOf("esota"+new AppTimestamp().currentTimeStamp()+new RandomGenerator().getRandomDigits());
                String stringImage = addProductBody.getProductImageDataList().get(i).getProductImageData().trim();

                String query_insertimage = "INSERT INTO products_images " +
                        "(id,product_id,image_url) \n" +
                        "VALUES (?,?,?)";

                PreparedStatement imaagePreparedStatement = con.prepareStatement(query_insertimage);
                imaagePreparedStatement.setInt(1, Values.EMPTY_OFF_VALUE);
                imaagePreparedStatement.setInt(2, insertedProductId);
                imaagePreparedStatement.setString(3,"productimage/"+imageName);
                imaagePreparedStatement.execute();
                try {
                    String extractedImageString[] = stringImage.split(",");
                    BufferedImage bi = ImageManager.decodeToImage(extractedImageString[1]);  // retrieve image : old->stringimage
                    //current path: /home/francis/Documents/software/springboot/backup/esotaapi/src/main/resources
                    //Old path: src/main/resources
                    File outputfile = new File("/home/francis/Documents/software/springboot/backup/esotaapi/src/main/resources/productimage/"+imageName+".jpg");
                    ImageIO.write(bi, "jpg", outputfile);
                } catch (IOException e) {
                    // handle exception
                }
            }

            message = addProductBody.getProductName()+" Added Sussessfuly";


        }catch (SQLException e) {
            message = "Failed";
        e.printStackTrace();
        }

        return new ProductListResponseBody(new Message(message));
    }

    public ProductListResponseBody editProduct(AddProductBody addProductBody){
        String message = null;

        Connection con = DatabaseConfiguration.getDatabeConnection();
        try {
            //verify user
            String query_updateProduct = "UPDATE products SET " +
                    "product_name = '"+addProductBody.getProductName()+"', " +
                    "quantity = '"+addProductBody.getQuantity()+"', " +
                    "selling_price = '"+addProductBody.getSellingPrice()+"', " +
                    "category_id = '"+addProductBody.getCategoryId()+"', " +
                    "description = '"+addProductBody.getDescription()+"' " +
                    "WHERE products.id = '"+addProductBody.getProductId()+"' AND products.shop_id='"+addProductBody.getShopId()+"'";

            Statement stmt = con.createStatement();
            stmt.executeUpdate(query_updateProduct);
            message = addProductBody.getProductName()+" Edited Successfuly";

        } catch (SQLException e) {
            e.printStackTrace();
        }

        return new ProductListResponseBody(new Message(message));
    }

}
