package com.esotacredit.esotaapi.api.shop.model.productlist;

import com.esotacredit.esotaapi.api.shop.objects.ItemShopProduct;
import com.esotacredit.esotaapi.api.shop.objects.ItemSoldProduct;
import com.esotacredit.esotaapi.api.shop.objects.Message;

import java.util.List;

public class ProductListResponseBody {
    private Message message;
    private List<ItemShopProduct> productList;
    private List<ItemSoldProduct> soldProductList;

    public ProductListResponseBody() {}

    public ProductListResponseBody(List<ItemShopProduct> productList) {
        this.productList = productList;
    }

    public ProductListResponseBody(Message message, List<ItemShopProduct> productList) {
        this.message = message;
        this.productList = productList;
    }

    public ProductListResponseBody(Message message) {
        this.message = message;
    }

    public ProductListResponseBody(List<ItemSoldProduct> soldProductList,int nullFlag) {
        this.soldProductList = soldProductList;
    }

    public List<ItemShopProduct> getProductList() {
        return productList;
    }

    public void setProductList(List<ItemShopProduct> productList) {
        this.productList = productList;
    }

    public List<ItemSoldProduct> getSoldProductList() {
        return soldProductList;
    }

    public Message getMessage() {
        return message;
    }
}
