package com.esotacredit.esotaapi.api.shop.objects;

import java.util.List;

public class ItemShopProduct {
    private int Id;
    private String productName;
    private String categoryName;
    private String sellingPrice;
    private int stockSize;
    private String description;
    private boolean approvalStatus;
    private List<ShopImage> shopImage;

    public ItemShopProduct() {}

    public ItemShopProduct(int id, String productName, String categoryName, String sellingPrice, int stockSize, String description, boolean approvalStatus) {
        Id = id;
        this.productName = productName;
        this.categoryName = categoryName;
        this.sellingPrice = sellingPrice;
        this.stockSize = stockSize;
        this.description = description;
        this.approvalStatus = approvalStatus;
    }

    public List<ShopImage> getShopImage() {
        return shopImage;
    }

    public void setShopImage(List<ShopImage> shopImage) {
        this.shopImage = shopImage;
    }

    public ItemShopProduct(int id, String productName, String categoryName, String sellingPrice, int stockSize, String description, boolean approvalStatus, List<ShopImage> shopImage) {
        Id = id;
        this.productName = productName;
        this.categoryName = categoryName;
        this.sellingPrice = sellingPrice;
        this.stockSize = stockSize;
        this.description = description;
        this.approvalStatus = approvalStatus;
        this.shopImage = shopImage;
    }

    public int getId() {
        return Id;
    }

    public void setId(int id) {
        Id = id;
    }

    public String getProductName() {
        return productName;
    }

    public void setProductName(String productName) {
        this.productName = productName;
    }

    public String getCategoryName() {
        return categoryName;
    }

    public void setCategoryName(String categoryName) {
        this.categoryName = categoryName;
    }

    public String getSellingPrice() {
        return sellingPrice;
    }

    public void setSellingPrice(String sellingPrice) {
        this.sellingPrice = sellingPrice;
    }

    public int getStockSize() {
        return stockSize;
    }

    public void setStockSize(int stockSize) {
        this.stockSize = stockSize;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public boolean isApprovalStatus() {
        return approvalStatus;
    }

    public void setApprovalStatus(boolean approvalStatus) {
        this.approvalStatus = approvalStatus;
    }
}
