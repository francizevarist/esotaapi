package com.esotacredit.esotaapi.api.shop.model.productlist;

import com.esotacredit.esotaapi.api.shop.objects.ProductImageData;

import java.util.List;

public class AddProductBody {
    private int productId;
    private String productName;
    private int shopId;
    private int quantity;
    private String sellingPrice;
    private int categoryId;
    private String description;
    private int countryId;
    private List<ProductImageData> productImageDataList;

    public AddProductBody() {}

    public AddProductBody(String productName, int shopId, int quantity, String sellingPrice, int categoryId, String description, int countryId, List<ProductImageData> productImageDataList) {
        this.productName = productName;
        this.shopId = shopId;
        this.quantity = quantity;
        this.sellingPrice = sellingPrice;
        this.categoryId = categoryId;
        this.description = description;
        this.countryId = countryId;
        this.productImageDataList = productImageDataList;
    }

    public AddProductBody(int productId, String productName, int shopId, int quantity, String sellingPrice, int categoryId, String description) {
        this.productId = productId;
        this.productName = productName;
        this.shopId = shopId;
        this.quantity = quantity;
        this.sellingPrice = sellingPrice;
        this.categoryId = categoryId;
        this.description = description;
    }

    public String getProductName() {
        return productName;
    }

    public void setProductName(String productName) {
        this.productName = productName;
    }

    public int getShopId() {
        return shopId;
    }

    public void setShopId(int shopId) {
        this.shopId = shopId;
    }

    public int getQuantity() {
        return quantity;
    }

    public void setQuantity(int quantity) {
        this.quantity = quantity;
    }

    public String getSellingPrice() {
        return sellingPrice;
    }

    public void setSellingPrice(String sellingPrice) {
        this.sellingPrice = sellingPrice;
    }

    public int getCategoryId() {
        return categoryId;
    }

    public void setCategoryId(int categoryId) {
        this.categoryId = categoryId;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public int getCountryId() {
        return countryId;
    }

    public void setCountryId(int countryId) {
        this.countryId = countryId;
    }

    public List<ProductImageData> getProductImageDataList() {
        return productImageDataList;
    }

    public void setProductImageDataList(List<ProductImageData> productImageDataList) {
        this.productImageDataList = productImageDataList;
    }

    public int getProductId() {
        return productId;
    }

    public void setProductId(int productId) {
        this.productId = productId;
    }
}
