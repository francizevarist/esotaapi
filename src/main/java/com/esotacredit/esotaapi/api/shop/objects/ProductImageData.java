package com.esotacredit.esotaapi.api.shop.objects;

public class ProductImageData {
    private String productImageData;

    public ProductImageData() {}

    public ProductImageData(String productImageData) {
        this.productImageData = productImageData;
    }

    public String getProductImageData() {
        return productImageData;
    }

    public void setProductImageData(String productImageData) {
        this.productImageData = productImageData;
    }

}
