package com.esotacredit.esotaapi.api.shop.model.productlist;

import com.esotacredit.esotaapi.api.shop.objects.ItemShopProduct;
import com.esotacredit.esotaapi.api.shop.objects.ItemSoldProduct;
import com.esotacredit.esotaapi.api.shop.objects.Message;

import java.util.List;

public class ProductListResponse {
    private Message message;
    private List<ItemShopProduct> productList;
    private List<ItemSoldProduct> soldProductList;

    public ProductListResponse(ProductListResponseBody productListResponseBody) {
        this.productList = productListResponseBody.getProductList();
        this.message = productListResponseBody.getMessage();
        this.soldProductList = productListResponseBody.getSoldProductList();
    }

    public Message getMessage() {
        return message;
    }

    public List<ItemShopProduct> getProductList() {
        return productList;
    }

    public List<ItemSoldProduct> getSoldProductList() {
        return soldProductList;
    }
}
